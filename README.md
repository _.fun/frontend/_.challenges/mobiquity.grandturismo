# Simple template for fast test projects creation

## Description

The application that uses [ergast](http://ergast.com/mrd/) api to show list of winners in races for 2005-2015 seasons. Champions marked with a globe.

### Use

```shell
yarn install
yarn run
```

Genric as possible.

## Components used

It's based on react with redux state management.

### UI

As ui framework was chosen [antd](https://github.com/ant-design/ant-design). It has wonderful documentation and I had no problem in using it.

### Routing

Just as an example it has simple routing with parameter. There won't be any issue in implementing custom routes.

## ToDo

- Tests
- Problems with `linter`.
- Needs better loading of data from api (should be on `componentDidMount`, but doesn't run on rerender for some reason)
- Some wierd issue with unique key for table
- All data comming from `api` should be reprocessed to lower camel case (I was lazy, there are helpers for that and can be integrated globally for all get methods)

## API

Api is wierd `http://ergast.com/api/f1/2005/results/1.json` with `.json`. It really should use content type requested in headers, rather than requesting it as a parameter.

## Time

Most of my type was spent on use of new UI framework. As I get tests requests, I love to do something new, to learn something out of completing tests like this one.

~2h R&D of template and new UI Framework
~1h fighting with things not working, even though they should, as always
~3h implementing the task itself (nothing difficult in getting data from api, process data)

PS: You should really describe what `season champion` means, not all people have `domain` logic and can be confused about it. `Gran Turismo`, `Grid`, `Grid 2` had fun.
