import React from 'react';
import PropTypes from 'prop-types';

import { Table, Icon } from 'antd';

const columns = [{
  title: 'Championship',
  dataIndex: 'isWorldChampion',
  key: 'isWorldChampion',
  render: (value) =>
    (value
      ? <Icon type="global" />
      : <Icon type="trophy" /> )
}, {
  title: 'Season',
  dataIndex: 'season',
  key: 'season',
}, {
  title: 'Race',
  dataIndex: 'raceName',
  key: 'raceName',
}, {
  title: 'Round',
  dataIndex: 'round',
  key: 'round',
}, {
  title: 'First Name',
  dataIndex: 'givenName',
  key: 'givenName',
}, {
  title: 'Last Name',
  dataIndex: 'familyName',
  key: 'familyName',
}, {
  title: 'Points',
  dataIndex: 'points',
  key: 'points',
},
];

const Races = ({ list }) => (
  <Table columns={columns} dataSource={list} />
);

Races.propTypes = {
  list: PropTypes.arrayOf(PropTypes.shape({

  }))
};

Races.defaultProps = {
  list: null,
};

export default Races;
