import axios from 'axios';

const api = 'http://ergast.com/api';

export const getRaces = (year) => (
  axios.get(`${api}/f1/${year}/results/1.json`)
    .then(({ data: { MRData: { RaceTable: { Races } } } }) => Races)
    .then(list => list.map(({
      season,
      raceName,
      round,
      Results: [{
        Driver: { driverId, familyName, givenName },
        points,
      }],
    }) => ({
      season,
      raceName,
      round,
      driverId,
      familyName,
      givenName,
      points,
    })))
    .then(list => {
      let champions = { '': 0 };
      list.forEach(({ driverId, points }) => {
        champions[driverId] = champions[driverId]
          ? champions[driverId] + Number(points)
          : Number(points);
      });

      const champion = Object.keys(champions)
        .reduce((previous, current) =>
          (champions[previous] < champions[current]
            ? current
            : previous), '');

      return list.map(race => ({
        ...race,
        isWorldChampion: race.driverId === champion
      }));
    })
);
