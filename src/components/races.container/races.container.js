import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Spin } from 'antd';
import { getRacesByYear } from './races.actions';
import Races from './races/races';

class RacesContainer extends Component {
  render() {
    const {
      dispatch,
      match: { params: { yearRoute } },
      year,
      list
    } = this.props;

    if (year !== yearRoute) getRacesByYear(yearRoute, dispatch);

    return (
      <div>
        <Spin spinning={year !== yearRoute} >
          <Races list={list} />
        </Spin>
      </div>
    );
  }
}

RacesContainer.propTypes = {
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      yearRoute: PropTypes.string.isRequired,
    })
  }).isRequired,
  year: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.shape({}))
};

RacesContainer.defaultProps = {
  dispatch: () => {},
  match: { params: { yearRoute: 0 } },
  list: null,
};

export default connect(({
  races: {
    year,
    list,
  } }) => ({
  year,
  list,
}))(RacesContainer);
