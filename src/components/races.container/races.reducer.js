import { RACES_UPDATE } from './constants';

const initialState = {
  year: null,
  list: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case RACES_UPDATE:
      return {
        ...state,
        year: action.year,
        list: action.list,
      };

    default:
      return state;
  }
};
