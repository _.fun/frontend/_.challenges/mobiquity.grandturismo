import { getRaces } from './races.api';
import { RACES_UPDATE } from './constants';

const racesUpdate = (year, list) => ({
  type: RACES_UPDATE,
  year,
  list,
});

export const getRacesByYear = (year, dispatch) => (
  getRaces(year).then((list) => dispatch(racesUpdate(year, list)))
);
