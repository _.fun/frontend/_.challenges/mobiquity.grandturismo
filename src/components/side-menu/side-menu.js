import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { Link } from 'react-router-dom';
import { Layout, Menu, Icon } from 'antd';
import { updateCollapsed } from './side-menu.actions';

const { Sider } = Layout;
const SubMenu = Menu.SubMenu;

const years = Array(11).fill().map((e,i)=>i+2005);

const SideMenu = ({ dispatch, collapsed, location: { pathname } }) => (
  <Sider
    collapsible
    collapsed={collapsed}
    onCollapse={(x) => dispatch(updateCollapsed(x))}
  >
    <div className="logo" />
    <Menu
      theme="dark"
      selectedKeys={[pathname]}
      mode="inline"
    >
      <Menu.Item key="/">
        <Link to="/">
          <div>
            <Icon type="home" />
            <span>home</span>
          </div>
        </Link>
      </Menu.Item>
      <SubMenu
        key="/f1/"
        title={<span><Icon type="car" /><span>F1</span></span>}
      >
        {years.map((year) => (
          <Menu.Item key={`/f1/${year}`}>
            <Link to={`/f1/${year}`}>{year}</Link>
          </Menu.Item>
        ))}
      </SubMenu>
    </Menu>
  </Sider>
);

SideMenu.propTypes = {
  dispatch: PropTypes.func.isRequired,
  collapsed: PropTypes.bool.isRequired,
  selected: PropTypes.string,
};

SideMenu.defaultProps = {
  dispatch: () => {},
  collapsed: false,
  selected: null,
};

export default withRouter(connect(({
  sideMenu: {
    collapsed,
    selected,
  } }) => ({
  collapsed,
  selected,
}))(SideMenu));
